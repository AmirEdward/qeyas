@extends('layouts.app')

@section('content')
    <section class="banner inner-page">
        <div class="banner-img"><img src="{{ asset('images/banner/register-bannerImg.jpg') }}" alt=""></div>
        <div class="page-title">
            <div class="container">
                <h1 class="Tajawal-font">تفاصيل الكتاب</h1>
            </div>
        </div>
    </section>
    <section class="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{ route('home') }}">الرئيسية</a></li>
                <li><a href="{{ route('books.index') }}">الكتب الإلكترونية</a></li>
                <li>{{ $book->book_title }}</li>
            </ul>
        </div>
    </section>
    <div class="blog-page blog-details">
        @if(session('fail'))
            <div class="alert alert-danger">
                {{ session('fail') }}
            </div>
        @elseif(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="blog-slide">
                        <div class="img" style="min-height: 100px;">
                            <img src="{{ Voyager::image($book->book_photo, asset('images/blog/img1.jpg')) }}" alt="">
                            <div class="date">
                                <span class="fontsize-30">{{ $book->book_price }} $</span>
                            </div>
                        </div>
                        <div class="info">
                            <div class="name">
                                {{ $book->book_title }}
                            </div>
                            <div class="post-info">
                                <span><i class="fa fa-calendar"></i>{{ $book->release_date }}</span>
                            </div>
                            <p>{{ $book->book_description }}</p>
                            <div class="btn-block">
                                @if($book->book_price == 0 || (Auth::check() && Auth::user()->has_book($book->id)))
                                    <a href="{{ route('books.view', $book) }}" class="btn">تصفح الكتاب</a>
                                    <a href="{{ route('books.download', $book) }}" class="btn3"><i class="fa fa-download" aria-hidden="true"></i> تحميل الكتاب</a>
                                @else
                                    <a href="{{ route('books.buy', $book) }}" class="btn">اشترِ الكتاب</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection