@extends('layouts.app')
@section('content')
    <section class="banner inner-page">
        <div class="banner-img"><img src="{{ asset('images/banner/register-bannerImg.jpg') }}" alt=""></div>
        <div class="page-title">
            <div class="container">
                <h1 class="Tajawal-font">شراء كتاب</h1>
            </div>
        </div>
    </section>
    <section class="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{ route('home') }}">الرئيسية</a></li>
                <li><a href="{{ route('books.index') }}">الكتب الإلكترونية</a></li>
                <li>شراء كتاب</li>
            </ul>
        </div>
    </section>
    <section class="courses-view list-view">
        @if(session('fail'))
            <div class="alert alert-danger">
                {{ session('fail') }}
            </div>
        @elseif(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="container">
            <div class="row">
                <div class="test-price">
                    <h3 class="Tajawal-font fontsize-30">السعر : {{ $book->book_price }} دولار</h3>
                </div>
                <div class="tab">
                    <button class="tablinks active" onclick="openCity(event, 'visa')">
                        <img class="pay-imgs" src="{{ asset('images/visa.png') }}" alt="">
                        <span>باي بال</span>
                    </button>
                    {{--<button class="tablinks" onclick="openCity(event, 'master-card')">--}}
                    {{--<img class="pay-imgs" src="images/{{ asset('master-card.png') }}" alt="">--}}
                    {{--<span>ماستر كارد</span>--}}
                    {{--</button>--}}
                    {{--<button class="tablinks" onclick="openCity(event, 'Tokyo')">--}}
                    {{--<img class="pay-imgs" src="{{ asset('images/sadad.jpg') }}" alt="">--}}
                    {{--<span>سداد</span>--}}
                    {{--</button>--}}
                    {{--<button class="tablinks" onclick="openCity(event, 'Tokyo')">--}}
                    {{--<img class="pay-imgs" src="{{ asset('images/sadad.jpg') }}" alt="">--}}
                    {{--<span>نظام دفع من موبايلي</span>--}}
                    {{--</button>--}}
                </div>
                <div id="visa" class="tabcontent">
                    <form class="payment-form" action="{{ route('books.checkout') }}" method="get">
                        <h3 class="Tajawal-font">دفع باي بال : <img class="pay-imgs" src="{{ asset('images/visa.png') }}" alt=""></h3>
                        <div class="row col-50">
                            <input type="hidden" name="book_id" value="{{ $book->id }}">
                            {{--<div class="form-group col-md-12">--}}
                            {{--<label for="cname">Name on Card</label>--}}
                            {{--<input type="text" id="cname" name="cardname" placeholder="John More Doe">--}}
                            {{--</div>--}}
                            {{--<div class="form-group col-md-12">--}}
                            {{--<label for="ccnum">Credit card number</label>--}}
                            {{--<input type="text" id="ccnum" name="cardnumber" placeholder="1111-2222-3333-4444">--}}
                            {{--</div>--}}
                            {{--<div class="form-group col-md-6">--}}
                            {{--<label for="expmonth">Exp Month</label>--}}
                            {{--<input type="text" id="expmonth" name="expmonth" placeholder="September">--}}
                            {{--</div>--}}
                            {{--<div class="form-group col-md-6">--}}
                            {{--<label for="expyear">Exp Year</label>--}}
                            {{--<input type="text" id="expyear" name="expyear" placeholder="2018">--}}
                            {{--</div>--}}
                            {{--<div class="form-group col-md-12">--}}
                            {{--<label for="cvv">CVV</label>--}}
                            {{--<input type="text" id="cvv" name="cvv" placeholder="352">--}}
                            {{--</div>--}}
                            <div class="text-center">
                                <button type="submit" class="btn">ادفع الآن</button>
                            </div>
                        </div>
                    </form>
                </div>
                {{--<div id="master-card" class="tabcontent">--}}
                {{--<form class="payment-form" action="index.html" method="post">--}}
                {{--<h3 class="Tajawal-font">دفع بالماستر كارد:  <img class="pay-imgs" src="{{ asset('images/master-card.png') }}" alt=""></h3>--}}
                {{--<div class="row col-50">--}}
                {{--<div class="form-group col-md-12">--}}
                {{--<label for="cname">Name on Card</label>--}}
                {{--<input type="text" id="cname" name="cardname" placeholder="John More Doe">--}}
                {{--</div>--}}
                {{--<div class="form-group col-md-12">--}}
                {{--<label for="ccnum">Credit card number</label>--}}
                {{--<input type="text" id="ccnum" name="cardnumber" placeholder="1111-2222-3333-4444">--}}
                {{--</div>--}}
                {{--<div class="form-group col-md-6">--}}
                {{--<label for="expmonth">Exp Month</label>--}}
                {{--<input type="text" id="expmonth" name="expmonth" placeholder="September">--}}
                {{--</div>--}}
                {{--<div class="form-group col-md-6">--}}
                {{--<label for="expyear">Exp Year</label>--}}
                {{--<input type="text" id="expyear" name="expyear" placeholder="2018">--}}
                {{--</div>--}}
                {{--<div class="form-group col-md-12">--}}
                {{--<label for="cvv">CVV</label>--}}
                {{--<input type="text" id="cvv" name="cvv" placeholder="352">--}}
                {{--</div>--}}
                {{--<div class="text-center">--}}
                {{--<a href="quiz-intro.html" class="btn">إدفع الآن</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</form>--}}
                {{--</div>--}}
                {{--<div id="Tokyo" class="tabcontent">--}}
                {{--<form class="payment-form" action="index.html" method="post">--}}
                {{--<h3 class="Tajawal-font">دفع بسداد  <img class="pay-imgs" src="{{ asset('images/sadad.jpg') }}" alt=""></h3>--}}
                {{--<div class="row col-50">--}}
                {{--<div class="form-group col-md-12">--}}
                {{--<label for="cname">اسم مستخدم حساب سداد</label>--}}
                {{--<input type="text" id="cname" name="cardname" placeholder="أدخل اسم مستخدم حساب سداد هنا">--}}
                {{--</div>--}}
                {{--<div class="text-center">--}}
                {{--<a href="quiz-intro.html" class="btn">إدفع الآن</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</form>--}}
                {{--</div>--}}
            </div>
        </div>
    </section>
@endsection