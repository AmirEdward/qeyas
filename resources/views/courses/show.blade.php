@extends('layouts.app')

@section('content')
    <section class="banner inner-page">
        <div class="banner-img"><img src="{{ asset('images/banner/register-bannerImg.jpg') }}" alt=""></div>
        <div class="page-title">
            <div class="container">
                <h1 class="Tajawal-font">تفاصيل الدورة</h1>
            </div>
        </div>
    </section>
    <section class="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{ route('home') }}">الرئيسية</a></li>
                <li><a href="{{ route('courses.index') }}">الدورات التدريبية</a></li>
                <li><a href="{{ route('courses.show', $course) }}">تفاصيل الدورة</a></li>
            </ul>
        </div>
    </section>
    <div class="container">
        @if(session('message'))
            <div class="alert alert-info">{{ session('message') }}</div>
        @endif
    </div>
    <div class="course-details">
        <div class="container">
            <h2 class="Tajawal-font">{{ $course->course_title }}</h2>
            <div class="course-details-main">
                <div class="course-img">
                    <img src="{{ Voyager::image($course->course_photo) }}" alt="">
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="course-instructorInfo">
                            <div class="info-slide">
                                <div class="date"><i class="fa fa-clock-o"></i>{{ $course->hours }} ساعة</div>
                            </div>
                            <div class="info-slide"><i class="fa fa-calendar"></i>{{ $course->start }} - {{ $course->end }}</div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="btn-row">
                            <div class="price"><span>السعر : </span>{{ $course->course_price == 0 ? 'مجاني' : $course->course_price }} $</div>
                            {{--<a href="cart.html" class="btn"><i class="fa fa-cart-plus"></i>أضف إلى السلة</a>--}}
                        </div>
                    </div>
                </div>

            </div>
            <div class="info">
                <h4>تفاصيل الدورة</h4>
                <p>
                    {{ $course->course_description }}
                </p>
            </div>
            <div class="instructors">
                <h4>المدربين</h4>
                <div class="row">
                    @foreach($course->trainers as $trainer)
                        <div class="col-sm-4">
                            <div class="instructors-box">
                                <div class="img">
                                    <img src="{{ Voyager::image($trainer->image) }}" alt="" height="370px">
                                </div>
                                <div class="name">{{ $trainer->trainer_name }}</div>
                                {{--<div class="name">الرياضيات</div>--}}
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            <div class="course-form">
                @auth('web')
                    <button type="button" class="btn btn-lg" data-toggle="modal" data-target="#myModal">سجل الآن</button>
                @else
                    <a href="{{ route('login') }}" class="btn btn-lg">تسجيل الدخول لطلب الدورة</a>
                @endauth
            </div>
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title Tajawal-font">عنوان الدورة</h3>
                        </div>
                        <form action="{{ route('courses.request', $course) }}" method="post">
                            {{ csrf_field() }}
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">الإسم</label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="الإسم" required>
                                </div>
                                <div class="form-group">
                                    <label for="phone">رقم الهاتف</label>
                                    <input type="number" name="phone" class="form-control" id="phone" placeholder="رقم الهاتف" required>
                                </div>
                                <div class="form-group">
                                    <label for="message">الرسالة</label>
                                    <textarea class="form-control" name="message" id="message" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn">إرسال طلب حجز</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
