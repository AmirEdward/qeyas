@extends('layouts.app')

@section('content')
    <section class="banner inner-page">
        <div class="banner-img"><img src="{{ asset('images/banner/register-bannerImg.jpg') }}" alt=""></div>
        <div class="page-title">
            <div class="container">
                <h1 class="Tajawal-font">حسابي</h1>
            </div>
        </div>
    </section>
    <section class="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{ route('home') }}">الرئيسية</a></li>
                <li><a href="{{ route('user.profile') }}">حسابي</a></li>
            </ul>
        </div>
    </section>
    <div class="container">
        @if(session('message'))
            <div class="alert alert-info">
                {{ session('message') }}
            </div>
        @endif
    </div>
    <div class="my-accountPage">
        <div class="container">
            <div class="my-account">
                <div class="account-tab">
                    <ul>
                        <li class="active"><a href="javascript:void(0);" id="profile">حسابي</a></li>
                        <li><a href="javascript:void(0);" id="order">مشترياتي</a></li>
                        <li><a href="javascript:void(0);" id="changePassword">تغيير كلمة المرور</a></li>
                        <li><a href="javascript:void(0);" id="exams-results">نتائج الإختبارات السابقة</a></li>
                    </ul>
                </div>
                <div class="tab-content profile-con open">
                    <div class="personal-edit">
                        <a href="{{ route('user.edit-profile') }}"><i class="fa fa-pencil"></i><span>تعديل حسابي</span></a>
                    </div>
                    <div class="personal-information">
                        <div class="info-slide">
                            <p><span>الإسم :</span>{{ $user->name }}</p>
                        </div>
                        <div class="info-slide">
                            <p><span>رقم الهاتف :</span>{{ $user->phone }}</p>
                        </div>
                        <div class="info-slide">
                            <p><span>المرحلة الدراسية :</span>{{ $user->education_level ?? '' }}</p>
                        </div>
                        <div class="info-slide">
                            <p><span>المدينة :</span>{{ $user->user_city->city_name ?? '' }}</p>
                        </div>
                        <div class="info-slide">
                            <p><span>الجنس :</span>{{ $user->gender ? ($user->gender === 'm' ? 'ذكر' : 'أنثى') : '' }}</p>
                        </div>
                    </div>
                </div>
                <div class="tab-content order-con">
                    <h4 class="Tajawal-font fontsize-18">إختباراتي</h4>
                    @if($user->exams->count())
                        <table class="booking-viewTable">
                            @foreach($user->exams as $exam)
                                <tr>
                                    <th class="th-name">{{ $exam->title }}</th>
                                    <th>#3442</th>
                                    <th>{{ $exam->payment_date }}</th>
                                    <th>{{ $exam->is_paid() ? $exam->price : 'مجاني' }}</th>
                                    <th>
                                        <a class="enter-test" href="{{ route('exams.show', $exam->id) }}">دخول</a>
                                    </th>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <h5>لا يوجد اختبارات</h5>
                    @endif
                    <h4 class="Tajawal-font fontsize-18">كتبي</h4>
                    @if($user->books->count())
                        <table class="booking-viewTable books">
                            @foreach($user->books as $book)
                                <tr>
                                    <th class="th-name">{{ $book->book_title }}</th>
                                    <th>#9876</th>
                                    <th>{{ $book->payment_date }}</th>
                                    <th>{{ $book->price }}</th>
                                    <th>
                                        <a href="{{ route('books.download', $book) }}"><i class="fa fa-download fontsize-18" aria-hidden="true"></i></a>
                                        <a target="_blank" href="{{ route('books.view', $book) }}"><i class="fa fa-file-pdf-o fontsize-18" aria-hidden="true"></i></a>
                                    </th>
                                </tr>
                            @endforeach
                        </table>
                    @endif
                </div>
                <div class="tab-content changePassword-con">
                    <div class="change-password ">
                        <form action="{{ route('user.update_password') }}" method="post">
                            {{ csrf_field() }}
                            <div class="input-box">
                                <input type="password" name="current_password" placeholder="كلمة المرور الحالية" value="{{ ! is_null($user->social_id) && ! $user->password_changed ? $user->social_id : '' }}" {{ ! is_null($user->social_id) && ! $user->password_changed ? 'readonly': '' }}>
                            </div>
                            @if(! is_null($user->social_id) && ! $user->password_changed)
                                <div class="alert alert-info">
                                    تم التسجيل بواسطة احدى وسائل التواصل الاجتماعي .. تم وضع كلمة مرور افتراضية لك اذا كنت ترغب بتغييرها فاكتب كلمة المرور الجديدة
                                </div>
                            @endif>
                            <div class="input-box">
                                <input type="password" name="password" placeholder="كلمة المرور الجديدة">
                            </div>
                            <div class="input-box">
                                <input type="password" name="password_confirmation" placeholder="تأكيد كلمة المرور الجديدة">
                            </div>
                            <div class="submit-box">
                                <input type="submit" value="حفظ" class="btn">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-content exams-results-con">
                    <div class="row">
                        @foreach($user->exam_reports as $report)
                            <div class="col-md-4">
                                <div class="exam-result">
                                    <div class="card">
                                        <a href="{{ route('exams.report', $report) }}">
                                            <h4 class="Tajawal-font font-weight">{{ $report->exam->title }}</h4>
                                        </a>
                                        <p>
                                            <span>أعلى نتيجة :</span>
                                            <span>{{ $report->highest_result }}</span>
                                        </p>
                                        <p>
                                            <span>عدد مرات الدخول :</span>
                                            <span>{{ $report->num_tries }} مرات</span>
                                        </p>
                                        <p>
                                            <span>الوقت المنقضي :</span>
                                            <span>{{ $report->time_spent }}</span>
                                        </p>
                                        <p>
                                            <span>تاريخ آخر دخول للإمتحان :</span>
                                            <span>{{ $report->last_try }}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {{--<div class="col-md-4">--}}
                        {{--<div class="exam-result">--}}
                        {{--<div class="card">--}}
                        {{--<a href="exam-report.html">--}}
                        {{--<h4 class="Tajawal-font font-weight">إختبار القدرات العامة باللغة العربية</h4>--}}
                        {{--</a>--}}
                        {{--<p>--}}
                        {{--<span>أعلى نتيجة :</span>--}}
                        {{--<span>40 نقطة</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>عدد مرات الدخول :</span>--}}
                        {{--<span>3 مرات</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>الوقت المنقضي :</span>--}}
                        {{--<span>00 : 12 : 23</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>تاريخ آخر دخول للإمتحان :</span>--}}
                        {{--<span>22 مايو 2019</span>--}}
                        {{--</p>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4">--}}
                        {{--<div class="exam-result">--}}
                        {{--<div class="card">--}}
                        {{--<a href="exam-report.html">--}}
                        {{--<h4 class="Tajawal-font font-weight">إختبار القدرات العامة باللغة العربية</h4>--}}
                        {{--</a>--}}
                        {{--<p>--}}
                        {{--<span>أعلى نتيجة :</span>--}}
                        {{--<span>40 نقطة</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>عدد مرات الدخول :</span>--}}
                        {{--<span>3 مرات</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>الوقت المنقضي :</span>--}}
                        {{--<span>00 : 12 : 23</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>تاريخ آخر دخول للإمتحان :</span>--}}
                        {{--<span>22 مايو 2019</span>--}}
                        {{--</p>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                        {{--<div class="col-md-4">--}}
                        {{--<div class="exam-result">--}}
                        {{--<div class="card">--}}
                        {{--<a href="exam-report.html">--}}
                        {{--<h4 class="Tajawal-font font-weight">إختبار القدرات العامة باللغة العربية</h4>--}}
                        {{--</a>--}}
                        {{--<p>--}}
                        {{--<span>أعلى نتيجة :</span>--}}
                        {{--<span>40 نقطة</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>عدد مرات الدخول :</span>--}}
                        {{--<span>3 مرات</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>الوقت المنقضي :</span>--}}
                        {{--<span>00 : 12 : 23</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>تاريخ آخر دخول للإمتحان :</span>--}}
                        {{--<span>22 مايو 2019</span>--}}
                        {{--</p>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4">--}}
                        {{--<div class="exam-result">--}}
                        {{--<div class="card">--}}
                        {{--<a href="exam-report.html">--}}
                        {{--<h4 class="Tajawal-font font-weight">إختبار القدرات العامة باللغة العربية</h4>--}}
                        {{--</a>--}}
                        {{--<p>--}}
                        {{--<span>أعلى نتيجة :</span>--}}
                        {{--<span>40 نقطة</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>عدد مرات الدخول :</span>--}}
                        {{--<span>3 مرات</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>الوقت المنقضي :</span>--}}
                        {{--<span>00 : 12 : 23</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>تاريخ آخر دخول للإمتحان :</span>--}}
                        {{--<span>22 مايو 2019</span>--}}
                        {{--</p>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4">--}}
                        {{--<div class="exam-result">--}}
                        {{--<div class="card">--}}
                        {{--<a href="exam-report.html">--}}
                        {{--<h4 class="Tajawal-font font-weight">إختبار القدرات العامة باللغة العربية</h4>--}}
                        {{--</a>--}}
                        {{--<p>--}}
                        {{--<span>أعلى نتيجة :</span>--}}
                        {{--<span>40 نقطة</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>عدد مرات الدخول :</span>--}}
                        {{--<span>3 مرات</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>الوقت المنقضي :</span>--}}
                        {{--<span>00 : 12 : 23</span>--}}
                        {{--</p>--}}
                        {{--<p>--}}
                        {{--<span>تاريخ آخر دخول للإمتحان :</span>--}}
                        {{--<span>22 مايو 2019</span>--}}
                        {{--</p>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection