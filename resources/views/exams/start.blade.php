@extends('layouts.app')

@section('content')

    <section class="banner inner-page">
        <div class="banner-img"><img src="{{ asset('images/banner/register-bannerImg.jpg') }}" alt=""></div>
        <div class="page-title">
            <div class="container">
                <h1 class="Tajawal-font">{{ $exam->title }}</h1>
            </div>
        </div>
    </section>
    <section class="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{ route('categories.show', $exam->category) }}">القسم الفرعي</a></li>
                <li><a href="{{ route('exams.show', $exam) }}">تعليمات قبل الاختبار</a></li>
                <li>{{ $exam->title }}</li>
            </ul>
        </div>
    </section>
    <section class="quiz-view">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div id="countdown" style="direction: ltr"></div>
                    <div class="qustion-list">
                        @foreach($sections as $section)
                            @if($section->questions->count())
                                <div class="qustion-slide" id="section-{{ $loop->index + 1 }}-title">
                                    <div class="qustion-number">{{ $section->section_title }}</div>
                                    <span>{{ $section->questions->count() }}</span>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <!-- has class exam_en or exam_ar based on exam language -->
                <div class="col-sm-8 col-md-9 exam_{{ $exam->lang }}">
                    @if(count($errors->all()))
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('exams.post_exam') }}" method="post" id="exam-from">
                        {{ csrf_field() }}
                        <input type="hidden" name="exam_id" value="{{ $exam->id }}">
                        <input type="hidden" name="userSubmit" id="user_submit" value=0>
                        <input type="hidden" name="time_spent" id="time_spent">
                        <div class="qustion-main" id="questions">
                            <div class="qustion-box" id="question-box">
                                @foreach($sections as $section)
                                    @if($section->questions->count())
                                        <div class="sections" id="section-{{ $loop->index + 1 }}">
                                            <div class="fontsize-18" style="margin-bottom: 10px;">
                                                <b>{{ $section->section_title }}</b>
                                            </div>
                                            @foreach($section->questions as $question)

                                                <div class="qustion">
                                                    {{ switch_numbers(($loop->index+1), $exam->lang) }}
                                                    @if(isset($question->question_text))
                                                        {!! switch_numbers($question->question_text, $exam->lang) !!}
                                                    @endif
                                                    @if(isset($question->question_image))
                                                        <img class="q-img"
                                                             src="{{ Voyager::image($question->question_img, asset('images/blog/img1.jpg')) }}"
                                                             alt="">
                                                    @endif
                                                </div>
                                                @if(isset($question->paragraph))
                                                <!-- Button trigger modal -->
                                                    <div style="margin-top: 10px;">
                                                        <button type="button" class="btn" data-toggle="modal" data-target="#paragraph-{{ $question->id }}">
                                                            القطعة
                                                        </button>
                                                    </div>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="paragraph-{{ $question->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    @if($exam->lang == 'ar')
                                                                        {!! switch_numbers($question->paragraph, $exam->lang) !!}
                                                                    @else
                                                                    @endif
                                                                </div>
                                                                {{--<div class="modal-footer">--}}
                                                                {{--<button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">إغلاق</button>--}}
                                                                {{--</div>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="ans">
                                                    @foreach($question->options as $option)
                                                        <div class="ans-slide">
                                                            @php($lang = $exam->lang)
                                                            {{ ${'list_index_' . $exam->lang}[$loop->index + 1] }} -
                                                            <label class="label_radio" for="radio-{{ $option->id }}">
                                                                <input name="questions[{{ $question->id }}]answer" id="radio-{{ $option->id }}" value="{{ $option->id }}" type="radio" {{ old("radio-". $option->id) ? 'checked' : '' }}>
                                                                @if(isset($option->option_text))
                                                                    {!! switch_numbers($option->option_text, $exam->lang) !!}
                                                                @endif
                                                                @if(isset($option->option_img))
                                                                    <img src="{{ Voyager::image($option->option_img, asset('images/certificate-logo.jpg')) }}" alt="">
                                                                @endif
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <hr style="border:2px dashed gray">
                                            @endforeach
                                        </div>
                                    @endif
                                @endforeach
                                <div class="btn-slide" id="pagination">
                                    <a href="javascript:" class="btn" id="previous">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                    <a href="javascript:" class="btn" id="next">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                </div>
                                <div class="submit-quiz">
                                    <button type="submit" class="btn" id="submit">إنهاء الإختبار</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery.countdown.js') }}"></script>
    <script>
        let minutes = parseInt("{{ $exam->exam_duration }}");
        let time_start = new Date().getTime();

        $('#countdown').countdown({
            timestamp: new Date().getTime() + minutes * 60 * 1000,
            callback: function () {
                let now = new Date().getTime();
                if (now > this.timestamp) {
                    alert('تم انتهاء الوقت');
                    $('#exam-from').submit();
                    this.stopeed = true;
                }
            },
            stopeed: false
        });

        $(function () {
            // Check if the form submitted by user or by javascript
            $('#submit').on('click', function (e) {
                $('#user_submit').val(1);
            });

            let page = 1;
            let sections = $('.sections');
            let prev = $('#previous');
            let next = $('#next');

            // Add style classes to sections' labels
            function addFillClass() {
                for (let i = 1; i < page; i++) {
                    $('#section-' + i + '-title').addClass('fill');
                }
            }

            function addActiveClass(page) {
                $('#section-' + page + '-title').addClass('active');
            }

            // show/hide paragraph (if exist)
            $('body').on('click', '.toggleParagraph', function () {
                let id = $(this).data('id');
                let paragraph = $('#paragraph-' + id);
                paragraph.toggleClass('hidden');
                if (paragraph.hasClass('hidden')) {
                    $(this).text('اظهار القطعة');
                } else {
                    $(this).text('اخفاء القطعة');
                }
            });

            function hideSections() {
                sections.each(function (index, section) {
                    $(section).hide();
                    if ($('.sections#section-' + page).length) {
                        $('#section-' + page).fadeIn();
                    }
                });
            }

            // show/hide sections
            hideSections();
            addFillClass();
            addActiveClass(page);
            prev.hide();
            next.on('click', function () {
                page++;
                addActiveClass(page);
                prev.show();
                hideSections();
                addFillClass();
                if (page >= sections.length) {
                    $(this).hide();
                }
            });

            prev.on('click', function () {
                page--;
                next.show();
                hideSections();
                addFillClass();
                if (page <= 1) {
                    $(this).hide();
                }
            });

            // submit the form if time end
            $('form#exam-from').submit(function () {
                let time_end = new Date().getTime();
                let time_spent = (time_end - time_start) / 1000;
                $('#time_spent').val(time_spent);
            });
        });

    </script>
@endsection