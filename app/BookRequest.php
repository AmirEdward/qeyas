<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookRequest extends Model
{
    protected $fillable = [
        'user_id', 'book_id', 'status'
    ];

    protected $table = 'book_requests';
}
