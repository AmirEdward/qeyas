<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ExamRequest
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExamRequest query()
 * @mixin \Eloquent
 */
class ExamRequest extends Model
{
    protected $fillable =
        [
            'user_id','exam_id','status'
        ];
}
