<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Course
 *
 * @property int $id
 * @property int $order
 * @property string $course_title
 * @property string $course_photo
 * @property string $start_date
 * @property string $end_date
 * @property string $hours
 * @property string $course_price
 * @property string $course_description
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCourseDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCoursePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCoursePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCourseTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Course extends Model
{

    private $ar_month = [
        'Jan' => 'يناير',
        'Feb' => 'فبراير',
        'Mar' => 'مارس',
        'Apr' => 'أبريل',
        'May' => 'مايو',
        'Jun' => 'يونيو',
        'Jul' => 'يوليو',
        'Aug' => 'أغسطس',
        'Sep' => 'سبتمبر',
        'Oct' => 'أكتوبر',
        'Nov' => 'نوفمبر',
        'Dec' => 'ديسمبر',
    ];

    public function trainers() {
        return $this->belongsToMany(Trainer::class, 'course_trainer');
    }

    public function getStartAttribute() {
        $value = Carbon::parse($this->start_date);
        return "$value->day " . $this->ar_month[$value->shortEnglishMonth] . " $value->year";
    }
    public function getEndAttribute() {
        $value = Carbon::parse($this->end_date);
        return "$value->day " . $this->ar_month[$value->shortEnglishMonth] . " $value->year";
    }

    public function requests() {
        return $this->hasMany(CourseRequest::class);
    }
}
