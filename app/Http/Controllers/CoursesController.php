<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CoursesController extends Controller
{

    public function index() {
        $courses = Course::orderBy('order')->paginate(10);
        $title = 'الدورات التدريبية';
        return view('courses.index', compact('courses', 'title'));
    }

    public function show(Course $course) {
        $title = $course->course_title;
        return view('courses.show', compact('course', 'title'));
    }

    public function request(Request $request, Course $course) {
        $this->validate($request, [
            'name' => 'required|string|min:3',
            'phone' => 'required',
            'message' => 'nullable|string'
        ]);
        $user = Auth::user();
        $course->requests()->create([
            'user_id' => $user->id,
            'status' => 'awaiting',
            'user_name' => $request->name,
//            'user_email' => $user->email,
            'user_phone' => $request->phone,
            'message' => $request->message,
        ]);

        return back()->with('message', 'تم ارسال الطلب بنجاح');
    }

    public function search(Request $request) {
        $courses = new Course;
        if($request->free_course == 1){
            $courses = $courses->where('course_price', 0);
        }

        if($request->paid_course == 1){
            $courses = $courses->where('course_price', '!=', 0);
        }

        if(isset($request->q)){
            $courses->where('course_title', 'like', "%$request->q");
        }

        $courses = $courses->paginate(10)->appends($request->all());
        return view('courses.search', compact('courses'));
    }
}
