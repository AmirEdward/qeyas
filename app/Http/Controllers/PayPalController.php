<?php

namespace App\Http\Controllers;

use App\Book;
use App\BookRequest;
use App\Exam;
use App\ExamRequest;
use App\Payment;
use App\UserNotification;
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;

class PayPalController extends Controller
{
    protected $provider;
    public function __construct() {
        $this->provider = new ExpressCheckout();
    }

    public function checkout_book(Request $request) {
        if(! $request->has('book_id')){
            return back()->with('fail', 'هناك خطأ .. حاول مرة اخرى');
        }

        $book = Book::find($request->book_id);
        $payment = Payment::create([
            'invoice_id' => time().uniqid(),
            'user_id' => auth()->id(),
            'book_id' => $book->id,
            'price' => $book->book_price
        ]);

        $data = $this->getBook($book, $payment->invoice_id);

        $response = $this->provider->setExpressCheckout($data);
        $payment->update(['token' => $response['TOKEN']]);
        return redirect($response['paypal_link']);
    }

    public function checkout_book_success(Request $request) {
        $token = $request->token;
        $book = Book::find($request->book_id);
        $PayerID = $request->PayerID;
        $response = $this->provider->getExpressCheckoutDetails($token);
        $invoice_id = $response['INVNUM'];
        if(! in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])){
            return redirect()->route('books.buy', $book)->with('fail', 'خطأ في عملية الدفع,, حاول مرة اخرى');
        }
        $data = $this->getBook($book, $invoice_id);
        $payment_status = $this->provider->doExpressCheckoutPayment($data, $token, $PayerID);
        $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];
        if(strtolower($status) == 'completed' ){
            $payment = Payment::where('token', $request->token)->first();
            $payment->update(['status' => 'approved']);
            $title = 'عملية الدفع - '.$book->book_title;
            $msg = 'تمت عملية الشراء بنجاح';
            UserNotification::create([
                'type' => 'private',
                'user_id' => auth()->id(),
                'content' => $title . ' ' . $msg
            ]);
            BookRequest::create([
                'user_id' => $payment->user_id,
                'book_id' => $book->id,
                'status' => 'approved'
            ]);
            return redirect()->route('books.show', $book)->with('success', 'تم شراء الكتاب بنجاح');
        }
        return back()->with('fail', 'حدث خطأ');
    }

    public function getBook($book, $invoice) {
        return [
            'items' => [
                [
                    'name' => $book->book_title,
                    'price' => $book->book_price,
                    'desc' => 'شراء كتاب من قياس 2030',
                    'qty' => 1
                ]
            ],
            'invoice_id' => $invoice,
            'invoice_description' => 'شراء كتاب من قياس 2030',
            'total' => $book->book_price,
            'shipping_discount' => 0,
            'return_url' => route('books.checkout_success', ['book_id' => $book->id]),
            'cancel_url' => route('books.show', $book)
        ];
    }

    public function checkout_exam(Request $request) {
        if(! $request->has('exam_id')){
            return back()->with('fail', 'هناك خطأ .. حاول مرة اخرى');
        }

        $exam = Exam::find($request->exam_id);
        $payment = Payment::create([
            'invoice_id' => time().uniqid(),
            'user_id' => auth()->id(),
            'exam_id' => $exam->id,
            'price' => $exam->exam_price
        ]);


        $data = $this->getExam($exam, $payment->invoice_id);
        $provider = new ExpressCheckout();
        $response = $provider->setExpressCheckout($data);
        $payment->update(['token' => $response['TOKEN']]);
        return redirect($response['paypal_link']);
    }

    public function checkout_exam_success(Request $request) {
        $token = $request->token;
        $exam = Exam::find($request->exam_id);
        $PayerID = $request->PayerID;
        $response = $this->provider->getExpressCheckoutDetails($token);
        $invoice_id = $response['INVNUM'];
        if(! in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])){
            return redirect()->route('exams.buy', $exam)->with('fail', 'خطأ في عملية الدفع,, حاول مرة اخرى');
        }
        $data = $this->getExam($exam, $invoice_id);
        $payment_status = $this->provider->doExpressCheckoutPayment($data, $token, $PayerID);
        $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];
        if(strtolower($status) == 'completed' ){
            $payment = Payment::where('token', $request->token)->first();
            $payment->update(['status' => 'approved']);
            $title = 'عملية الدفع - '.$payment->exam->title;
            $msg = 'تمت عملية الشراء بنجاح';
            UserNotification::create([
                'type' => 'private',
                'user_id' => auth()->id(),
                'content' => $title . ' ' . $msg
            ]);
            ExamRequest::create
            (
                [
                    'user_id' => $payment->user_id,
                    'exam_id' => $payment->exam_id,
                    'status' => 'approved'
                ]
            );
            return redirect()->route('exams.show', $exam)->with('success', 'تم شراء الاختبار بنجاح');
        }
        return back()->with('fail', 'حدث خطأ');
    }

    public function getExam($exam, $invoice) {
        return [
            'items' => [
                [
                    'name' => $exam->title,
                    'price' => $exam->exam_price,
                    'desc' => 'شراء اختبار من قياس 2030',
                    'qty' => 1
                ]
            ],
            'invoice_id' => $invoice,
            'invoice_description' => 'شراء اختبار من قياس 2030',
            'total' => $exam->exam_price,
            'shipping_discount' => 0,
            'return_url' => route('exams.checkout_success', ['exam_id' => $exam->id]),
            'cancel_url' => route('exams.show', $exam)
        ];
    }
}
