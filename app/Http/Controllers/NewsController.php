<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index() {
        $title = 'أخبارنا';
        $news = News::where('active', 1)->paginate(10);
        return view('news.index', compact('news', 'title'));
    }

    public function show(News $news) {
        $title = $news->title;
        $news->num_watches +=1 ;
        $news->save();
        return view('news.show', compact('news', 'title'));
    }
}
