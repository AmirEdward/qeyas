<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

class BooksController extends Controller
{

    public function index() {
        $books = Book::paginate(10);
        $title = 'الكتب الالكترونية';
        return view('books.index', compact('books', 'title'));
    }

    public function show(Book $book) {
        $title = $book->book_title;
        return view('books.show', compact('book', 'title'));
    }

    public function view(Book $book) {
        if($book->book_price == 0 || \Auth::user()->has_book($book->id)) {
            return view('books.view', compact('book'));
        }
        return redirect()->route('books.buy', $book);
    }

    public function download(Book $book) {
        $url = storage_path("app/public/{$book->url}");
        if($book->book_price == 0 || \Auth::user()->has_book($book->id)) {
            if(file_exists($url)){
                return response()->download($url, "$book->book_title.pdf", [
                    'Content-Type: application/pdf',
                ]);
            }
            abort(404, 'الكتاب غير موجود');
        }
        return redirect()->route('books.buy', $book);
    }

    public function buy(Book $book) {
        if($book->book_price == 0 || \Auth::user()->has_book($book->id)){
            return redirect()->route('books.show', $book);
        }
        $title = 'شراء كتاب';
        return view('books.buy', compact('book', 'title'));
    }
}
