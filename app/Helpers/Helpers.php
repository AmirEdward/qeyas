<?php
function switch_module_title($_text){
    switch($_text){
        case "Categories":
            $_text = "الاقسام";
            break;
		case "Users":
            $_text = "المستخدمين";
            break;
		case "Settings":
            $_text = "الإعدادات";
            break;
		case "Exams":
            $_text = "الاختبارات";
            break;
		case "Exam-questions":
            $_text = "اسئلة الاختبارات";
            break;
		case "Question-options":
            $_text = "اختيارات الاسئلة";
            break;
		case "Courses":
            $_text = "الدورات التدريبية";
            break;
		case "Course-requests":
            $_text = "طلبات الدورات";
            break;
		case "Trainers":
            $_text = "المدربين";
            break;
		case "Books":
            $_text = "الكتب";
            break;
		case "Videos":
            $_text = "مكتبة الفيديو";
            break;
		case "User-notifications":
            $_text = "الاشعارات";
            break;
		case "Cities":
            $_text = "المدن";
            break;
		case "Adsense":
            $_text = "الاعلانات الترويجية";
            break;
		case "Payments":
            $_text = "المدفوعات";
            break;
		case "Contact-us":
            $_text = "رسائل التواصل";
            break;
		case "Qeyas-news":
            $_text = "اخبار قياس";
            break;
		case "Profile":
            $_text = "الملف الشخصي";
            break;
        case "Create":
            $_text = "اضافة جديد";
            break;
		case "Edit":
            $_text = "تعديل";
            break;
		case "Admin":
            $_text = "لوحة التحكم";
            break;
		case "Site":
            $_text = "الموقع الالكتروني";
            break;
		case "App":
            $_text = "تطبيقات الموبايل";
            break;
		case "Exam_questions":
            $_text = "اسئلة الاختبارات";
            break;
        case "Exam_sections":
            $_text = "اقسام الاختبارات";
            break;
        default:
            $_text = $_text;
            break;
    }
    return $_text;
}

function switch_numbers($str, $lang){
    $eastern_arabic = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');
    $western_arabic = array('0','1','2','3','4','5','6','7','8','9');
    if($lang == 'ar'){
        $str = str_replace($western_arabic, $eastern_arabic, $str);
    }else {
        $str = str_replace($eastern_arabic, $western_arabic, $str);
    }
    return $str;
}

