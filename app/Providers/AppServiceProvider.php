<?php

namespace App\Providers;

use App\Category;
use App\Notifications\UserCreatedNotification;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Facades\Voyager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
        foreach (glob(app_path('Helpers') . '/*.php') as $file) {
            require_once $file;
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /* Add action buttons to the admin panel */
        Voyager::addAction(\App\Actions\ExamQuestions::class);
        Voyager::addAction(\App\Actions\ExamSections::class);

        $categories = Category::where('cat_parent', 0)
            ->orderBy('cat_order')
            ->take(4)
            ->get();
        $ar_month = [
            'Jan' => 'يناير',
            'Feb' => 'فبراير',
            'Mar' => 'مارس',
            'Apr' => 'أبريل',
            'May' => 'مايو',
            'Jun' => 'يونيو',
            'Jul' => 'يوليو',
            'Aug' => 'أغسطس',
            'Sep' => 'سبتمبر',
            'Oct' => 'أكتوبر',
            'Nov' => 'نوفمبر',
            'Dec' => 'ديسمبر',
        ];
        $list_index_ar = ['', 'أ', 'ب', 'ج', 'د', 'هـ', 'و', 'ز', 'ح', 'ط', 'ي', 'ك', 'ل', 'م', 'ن'];
        $list_index_en = ['', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n'];
        view()->share(['categories' => $categories, 'ar_month' => $ar_month, 'list_index_ar' => $list_index_ar, 'list_index_en' => $list_index_en]);
    }
}
