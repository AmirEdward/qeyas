<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Book
 *
 * @property int $id
 * @property string $book_title
 * @property string $book_photo
 * @property string $book_description
 * @property string $book_price
 * @property string $book_release_date
 * @property string $book_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookPhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookReleaseDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereBookUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Book whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Book extends Model
{
    protected $table = 'books';
    private $ar_month = [
        'Jan' => 'يناير',
        'Feb' => 'فبراير',
        'Mar' => 'مارس',
        'Apr' => 'أبريل',
        'May' => 'مايو',
        'Jun' => 'يونيو',
        'Jul' => 'يوليو',
        'Aug' => 'أغسطس',
        'Sep' => 'سبتمبر',
        'Oct' => 'أكتوبر',
        'Nov' => 'نوفمبر',
        'Dec' => 'ديسمبر',
    ];

    public function getReleaseDateAttribute() {
        $value = Carbon::parse($this->book_release_date);
        return "$value->day " . $this->ar_month[$value->shortEnglishMonth] . " $value->year";
    }

    public function getUrlAttribute() {
        return json_decode($this->book_url)[0]->download_link;
    }

    public function user_payment() {
        return $this->hasOne(Payment::class)->where('user_id', \Auth::id() ?? 0);
    }

    public function getPaymentDateAttribute() {
        $date = $this->user_payment->created_at;
        return "$date->day " . $this->ar_month[$date->shortEnglishMonth] . " $date->year";
    }
}
