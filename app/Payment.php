<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * App\Payment
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $admin_id
 * @property int $price
 * @property int|null $book_id
 * @property int|null $exam_id
 * @property string $payment_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereExamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment wherePaymentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUserId($value)
 * @mixin \Eloquent
 */
class Payment extends Model
{
    protected $fillable = ['status', 'token', 'invoice_id', 'user_id', 'exam_id', 'book_id', 'admin_id', 'price'];

    protected static function boot() {
        parent::boot();
        self::creating(function($model){
            $model->admin_id = Auth::id() ?? null;
        });
    }


    public function exam()
    {
        return $this->belongsTo(Exam::class,'exam_id');
    }



}
